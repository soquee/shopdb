package main

import (
	"log"
	"net/http"
	"net/http/pprof"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/collectors"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

// newMetrics returns a new metrics registry and an HTTP server that will serve
// information about the registry on /debug/metrics.
// If any additional registries (gatherers) are provided, they are included in
// statistics exposed on the /debug/metrics endpoint.
// If usepprof is true, then /debug/pprof and related endpoints are also
// registered.
func newMetrics(
	usepprof bool,
	debugListen string,
	logger *log.Logger,
	gatherers ...prometheus.Gatherer,
) (*prometheus.Registry, *http.Server) {
	metrics := prometheus.NewPedanticRegistry()
	metrics.MustRegister(
		collectors.NewProcessCollector(collectors.ProcessCollectorOpts{
			Namespace: Namespace,
		}),
		collectors.NewGoCollector(),
	)

	g := make(prometheus.Gatherers, 0, len(gatherers)+1)
	g = append(g, metrics)
	g = append(g, gatherers...)
	debugMux := http.NewServeMux()
	debugMux.Handle("/debug/metrics", promhttp.HandlerFor(g, promhttp.HandlerOpts{
		ErrorLog:      logger,
		ErrorHandling: promhttp.PanicOnError,
	}))
	if usepprof {
		debugMux.HandleFunc("/debug/pprof/", pprof.Index)
		debugMux.HandleFunc("/debug/pprof/cmdline", pprof.Cmdline)
		debugMux.HandleFunc("/debug/pprof/profile", pprof.Profile)
		debugMux.HandleFunc("/debug/pprof/symbol", pprof.Symbol)
		debugMux.HandleFunc("/debug/pprof/trace", pprof.Trace)
	}

	return metrics, &http.Server{
		Addr:              debugListen,
		Handler:           debugMux,
		ErrorLog:          logger,
		ReadTimeout:       3 * time.Minute,
		WriteTimeout:      3 * time.Minute,
		ReadHeaderTimeout: 30 * time.Second,
	}
}
