-- Helpers

CREATE FUNCTION public.manage_updated_at(_tbl regclass) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
    EXECUTE format('CREATE TRIGGER set_updated_at BEFORE UPDATE ON %s
                    FOR EACH ROW EXECUTE PROCEDURE set_updated_at()', _tbl);
END;
$$;

CREATE FUNCTION public.set_updated_at() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    IF (
        NEW IS DISTINCT FROM OLD AND
        NEW.updated_at IS NOT DISTINCT FROM OLD.updated_at
    ) THEN
        NEW.updated_at := current_timestamp;
    END IF;
    RETURN NEW;
END;
$$;

-- BE ADVISED: this function is its own inverse and is *not* a security measure.
-- If you're ever tempted to use this as a security measure: don't. This is just
-- a nicer way to format work order numbers because "00000001" and "00000002"
-- just felt weird and from a business perspective customers may not want it to
-- be immediately obvious to any casual customer how much business they've done
-- in a week.
--
-- https://wiki.postgresql.org/wiki/Pseudo_encrypt
CREATE OR REPLACE FUNCTION pseudo_encrypt(value int) returns int AS $$
DECLARE
l1 int;
l2 int;
r1 int;
r2 int;
i int:=0;
BEGIN
	l1:= (value >> 16) & 65535;
	r1:= value & 65535;
	WHILE i < 3 LOOP
		l2 := r1;
		r2 := l1 # ((((1366 * r1 + 150889) % 714025) / 714025.0) * 32767)::int;
		l1 := l2;
		r1 := r2;
		i := i + 1;
	END LOOP;
	return ((r1 << 16) + l1);
END;
$$ LANGUAGE plpgsql strict immutable;

CREATE OR REPLACE FUNCTION pseudo_encrypt_bigint(VALUE bigint) returns bigint AS $$
DECLARE
l1 bigint;
l2 bigint;
r1 bigint;
r2 bigint;
i int:=0;
BEGIN
	l1:= (VALUE >> 32) & 4294967295::bigint;
	r1:= VALUE & 4294967295;
	WHILE i < 3 LOOP
		l2 := r1;
		r2 := l1 # ((((1366.0 * r1 + 157249) % 714025) / 714025.0) * 32767*32767)::int;
		l1 := l2;
		r1 := r2;
		i := i + 1;
END LOOP;
RETURN ((r1::bigint << 32) + l1);
END;
$$ LANGUAGE plpgsql strict immutable;

--
-- System
--

CREATE TABLE IF NOT EXISTS "users" (
	"id"           INTEGER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	"username"     TEXT    NOT NULL UNIQUE CHECK (username <> ''),
	"displayname"  TEXT    NOT NULL        CHECK (displayname <> ''),
	"email"        TEXT    NOT NULL UNIQUE CHECK (email <> ''),
	"email_secret" BYTEA   NOT NULL,
	"verified"     BOOLEAN NOT NULL DEFAULT FALSE,
	"salt"         BYTEA   NOT NULL,
	"passhash"     BYTEA   NOT NULL,
	"hashscheme"   INTEGER NOT NULL,
	"totp_enabled" BOOLEAN NOT NULL DEFAULT FALSE,
	"totp_secret"  BYTEA   NOT NULL CHECK (octet_length(totp_secret) >= 32),
	"created_at"   TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
	"updated_at"   TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);
SELECT manage_updated_at('users');

CREATE SEQUENCE logins_id_seq
	AS BIGINT
	MINVALUE   1
	START WITH 1
	NO CYCLE;
CREATE TABLE IF NOT EXISTS "logins" (
	"id"         BIGINT    PRIMARY KEY    DEFAULT pseudo_encrypt_bigint(nextval('logins_id_seq')),
	"owner"      INTEGER   NOT NULL,
	"mac"        TEXT      NOT NULL,
	"ip"         TEXT      NOT NULL       DEFAULT '',
	"ua"         TEXT      NOT NULL       DEFAULT '',
	"created_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
	"updated_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,

	FOREIGN KEY(owner) REFERENCES users(id)
);
SELECT manage_updated_at('logins');
ALTER SEQUENCE logins_id_seq OWNED BY logins.id;

--
-- Services
--

CREATE TYPE contactpref AS ENUM ('call', 'sms', 'email');
CREATE TABLE IF NOT EXISTS "customers" (
	"id"         INTEGER     GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	"num"        INTEGER     GENERATED ALWAYS AS (pseudo_encrypt(id)) STORED,
	"name"       TEXT        NOT NULL,
	"phone"      TEXT,
	"email"      TEXT,
	"addr"       TEXT,
	"note"       TEXT        NOT NULL DEFAULT '',
	"flag"       BOOLEAN     NOT NULL DEFAULT FALSE,
	"contact"    contactpref NOT NULL,
	"created_at" TIMESTAMP   WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
	"updated_at" TIMESTAMP   WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,


	CHECK (phone IS NOT NULL OR email IS NOT NULL)
);
SELECT manage_updated_at('customers');

CREATE TABLE IF NOT EXISTS "bikes" (
	"id"         INTEGER   GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	"num"        INTEGER   GENERATED ALWAYS AS (pseudo_encrypt(id)) STORED,
	"make"       TEXT      CHECK (make <> ''),
	"model"      TEXT      CHECK (model <> ''),
	"color"      TEXT      NOT NULL CHECK (color <> ''),
	"sn"         TEXT      UNIQUE   CHECK (sn <> ''),
	"customer"   INTEGER   NOT NULL,
	"created_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
	"updated_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,

	FOREIGN KEY(customer) REFERENCES customers(id)
);
SELECT manage_updated_at('bikes');

CREATE TYPE wostatus AS ENUM (
	'work_new',
	'work_queue',
	'work_started',
	'work_done',
	'work_pickedup',
	'work_canceled_customer',
	'work_canceled_us',
	'delay_parts',
	'delay_customer',
	'delay_warranty',
	'delay_other'
);
CREATE TABLE IF NOT EXISTS "workorders" (
	"id"         INTEGER   GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	"num"        INTEGER   GENERATED ALWAYS AS (pseudo_encrypt(id)) STORED,
	"status"     wostatus  NOT NULL,
	"bike"       INTEGER,
	"customer"   INTEGER   NOT NULL,
	"req"        TEXT      NOT NULL DEFAULT '',
	"note"       TEXT      NOT NULL DEFAULT '',
	"created_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
	"updated_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,

	FOREIGN KEY(bike) REFERENCES bikes(id),
	FOREIGN KEY(customer) REFERENCES customers(id)
);
SELECT manage_updated_at('workorders');

CREATE TABLE IF NOT EXISTS "files" (
	"id"         INTEGER   GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	"filename"   TEXT      NOT NULL CHECK (filename <> ''),
	"wo"         INTEGER   NOT NULL,
	"created_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,

	FOREIGN KEY(wo) REFERENCES workorders(id)
);

CREATE TABLE IF NOT EXISTS "workordernotes" (
	"id"         INTEGER   GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	"note"       TEXT      NOT NULL CHECK (note <> ''),
	"wo"         INTEGER   NOT NULL,
	"created_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,

	FOREIGN KEY(wo) REFERENCES workorders(id)
);

CREATE TABLE IF NOT EXISTS "services" (
	"id"         INTEGER   GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	"name"       TEXT      NOT NULL CHECK (name <> ''),
	"cost"       INTEGER   NOT NULL CHECK (cost >= 0),
	"dur"        INTEGER   CHECK (dur >= 0),
	"hourly"     BOOLEAN   NOT NULL DEFAULT FALSE,
	"expl"       TEXT      NOT NULL CHECK (expl <> ''),
	"archived"   BOOLEAN   NOT NULL DEFAULT FALSE,
	"created_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
	"updated_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);
SELECT manage_updated_at('services');
CREATE UNIQUE INDEX unarchived_services ON services (name) WHERE (archived IS FALSE);

CREATE TABLE IF NOT EXISTS "inventory" (
	"id"         INTEGER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	"name"       TEXT      NOT NULL       CHECK (name <> ''),
	"barcode"    TEXT      NOT NULL       CHECK (barcode <> ''),
	"cost"       INTEGER   NOT NULL       CHECK (cost > 0),
	"variable"   BOOLEAN   NOT NULL       DEFAULT FALSE,
	"archived"   BOOLEAN   NOT NULL       DEFAULT FALSE,
	"created_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
	"updated_at" TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);
SELECT manage_updated_at('inventory');
CREATE UNIQUE INDEX unarchived_inventory
	ON inventory (barcode)
	WHERE (archived IS FALSE);

CREATE TYPE discounttype AS ENUM ('percent', 'amount');
CREATE TABLE IF NOT EXISTS "servicelineitems" (
	"id"          INTEGER                  GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	"wo"          INTEGER                  NOT NULL,
	"service"     INTEGER                  NOT NULL,
	"dur"         INTEGER                  CHECK (dur >= 0),
	"discount"    discounttype,
	"final_cost"  INTEGER                  CHECK (final_cost >= 0),
	"created_at"  TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
	"updated_at"  TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,

	CHECK ((discount IS NOT NULL AND final_cost IS NOT NULL) OR (discount IS NULL AND final_cost IS NULL)),
	FOREIGN KEY(wo)      REFERENCES workorders(id),
	FOREIGN KEY(service) REFERENCES services(id)
);
SELECT manage_updated_at('servicelineitems');

CREATE TABLE IF NOT EXISTS "partlineitems" (
	"id"          INTEGER                  GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	"wo"          INTEGER                  NOT NULL,
	"item"        INTEGER                  NOT NULL,
	"cost"        INTEGER                  CHECK (cost >= 0),
	"discount"    discounttype,
	"final_cost"  INTEGER                  CHECK (final_cost >= 0),
	"created_at"  TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
	"updated_at"  TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,

	CHECK ((discount IS NOT NULL AND final_cost IS NOT NULL) OR (discount IS NULL AND final_cost IS NULL)),
	FOREIGN KEY(wo)      REFERENCES workorders(id),
	FOREIGN KEY(item)    REFERENCES inventory(id)
);
SELECT manage_updated_at('partlineitems');
