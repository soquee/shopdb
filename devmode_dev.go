//go:build dev
// +build dev

package main

import (
	"io/fs"
	"os"
)

// Dev is set at compile time by the "dev" build tag and indicates whether the
// binary was built in dev mode or not.
const Dev = true

// In dev mode, load migrations directly from the file system.
func getMigrationsFS() fs.FS {
	vfs := os.DirFS("migrations")
	return vfs
}
