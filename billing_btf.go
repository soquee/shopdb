//go:build btf
// +build btf

package main

// BTF is set at compile time by the "btf" build tag and indicates whether the
// binary was built in Behind-the-Firewall mode or not.
const BTF = true
