package store

import (
	"context"
	"database/sql"
	"errors"
	"fmt"

	"code.soquee.net/shopdb/internal/graph/model"
)

// Customers performs operations on the customers table.
type Customers struct {
	db                *sql.DB
	getCustomer       *sql.Stmt
	getCustomerByBike *sql.Stmt
	getCustomerByWO   *sql.Stmt

	newCustomer *sql.Stmt
}

// NewCustomers creates a new Customer that performs its operations on the given
// database.
func NewCustomers(ctx context.Context, db *sql.DB) (Customers, error) {
	var err error
	store := Customers{
		db: db,
	}

	store.getCustomer, err = db.PrepareContext(ctx, `
SELECT num,name,phone,email,addr,note,flag,contact,created_at,updated_at FROM customers WHERE num=$1`)
	if err != nil {
		return store, fmt.Errorf("error preparing getCustomer: %q", err)
	}
	store.getCustomerByBike, err = db.PrepareContext(ctx, `
SELECT c.num,c.name,c.phone,c.email,c.addr,c.note,c.flag,c.contact,c.created_at,c.updated_at
	FROM customers c
	LEFT JOIN bikes AS b ON (c.id=b.customer)
	WHERE b.num=$1`)
	if err != nil {
		return store, fmt.Errorf("error preparing getOwners: %q", err)
	}
	store.getCustomerByWO, err = db.PrepareContext(ctx, `
SELECT c.num,c.name,c.phone,c.email,c.addr,c.note,c.flag,c.contact,c.created_at,c.updated_at
	FROM customers c
	LEFT JOIN workorders AS wo ON (c.id=wo.customer)
	WHERE wo.num=$1`)
	if err != nil {
		return store, fmt.Errorf("error preparing getCustomer: %q", err)
	}
	store.newCustomer, err = db.PrepareContext(ctx, `
INSERT INTO customers (name, phone, email, addr, note, flag, contact)
	VALUES ($1, $2, $3, $4, COALESCE($5, ''), COALESCE($6, FALSE), $7)
	RETURNING num, name, phone, email, addr, note, flag, contact, created_at, updated_at;
`)
	if err != nil {
		return store, fmt.Errorf("error preparing newCustomer: %q", err)
	}

	return store, nil
}

// DB returns the database that the store was created with.
func (s Customers) DB() *sql.DB {
	return s.db
}

// GetCustomer returns the given customer or nil if no such customer exists.
func (s Customers) GetCustomer(ctx context.Context, num int) (*model.Customer, error) {
	cus := &model.Customer{}
	err := s.getCustomer.QueryRowContext(ctx, num).Scan(
		&cus.ID, &cus.Name, &cus.Phone, &cus.Email, &cus.Addr, &cus.Note,
		&cus.Flagged, &cus.Contact, &cus.Created, &cus.Updated)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return nil, nil
	}
	return cus, err
}

// GetCustomerByBike returns the owner of the given bike.
func (s Customers) GetCustomerByBike(ctx context.Context, num int) (*model.Customer, error) {
	cus := &model.Customer{}
	err := s.getCustomerByBike.QueryRowContext(ctx, num).Scan(
		&cus.ID, &cus.Name, &cus.Phone, &cus.Email, &cus.Addr, &cus.Note,
		&cus.Flagged, &cus.Contact, &cus.Created, &cus.Updated)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return nil, nil
	}
	return cus, err
}

// GetCustomerByWO returns the customer on the given work order or nil if no
// such customer or work order exists.
func (s Customers) GetCustomerByWO(ctx context.Context, wo int) (*model.Customer, error) {
	cus := &model.Customer{}
	err := s.getCustomerByWO.QueryRowContext(ctx, wo).Scan(
		&cus.ID, &cus.Name, &cus.Phone, &cus.Email, &cus.Addr, &cus.Note,
		&cus.Flagged, &cus.Contact, &cus.Created, &cus.Updated)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return nil, nil
	}
	return cus, err
}

// CreateCustomer inserts a new customer and returns the resulting value.
func (s Customers) CreateCustomer(ctx context.Context, customer model.NewCustomer) (*model.Customer, error) {
	cus := &model.Customer{}
	err := s.newCustomer.
		QueryRowContext(ctx, customer.Name, customer.Phone, customer.Email,
			customer.Addr, customer.Note, customer.Flagged, customer.Contact).
		Scan(&cus.ID, &cus.Name, &cus.Phone, &cus.Email, &cus.Addr, &cus.Note,
			&cus.Flagged, &cus.Contact, &cus.Created, &cus.Updated)
	return cus, err
}
