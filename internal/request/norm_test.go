package request_test

import (
	"net/http"
	"net/http/httptest"
	"net/url"
	"strconv"
	"strings"
	"testing"
	"unicode/utf8"

	"golang.org/x/text/unicode/rangetable"
	"golang.org/x/text/unicode/runenames"

	"code.soquee.net/mux"
	"code.soquee.net/shopdb/internal/request"
)

func TestURLProfile(t *testing.T) {
	assigned := rangetable.Assigned(request.UnicodeVersion)
	rangetable.Visit(assigned, func(r rune) {
		s, err := request.URLProfile.String(string(r))
		if err != nil {
			return
		}
		rr, _ := utf8.DecodeRuneInString(s)
		if strings.ContainsRune(request.Disallowed, rr) {
			t.Errorf("Codepoint %U (%s) resolved to disallowed character after NFC: %q", r, runenames.Name(r), s)
		}
	})
}

var middlewareTestCases = [...]struct {
	route    string
	path     string
	redirect string
	code     int
}{
	0: {
		route: "/{org string}",
		path:  "/path",
		code:  http.StatusOK,
	},
	1: {
		route: "/{project string}",
		path:  "/path",
		code:  http.StatusOK,
	},
	2: {
		route: "/{username string}",
		path:  "/path",
		code:  http.StatusOK,
	},
	3: {
		route: "/{org string}",
		path:  "/\u26D6", // bidirule.ErrInvalid
		code:  http.StatusBadRequest,
	},
	4: {
		route:    "/{org string}",
		path:     "/\u0063\u0327", // c + cedille
		code:     http.StatusPermanentRedirect,
		redirect: "/ç",
	},
	5: {
		route:    "/{org string}/{project string}/{username string}/",
		path:     "/\u0063\u0327/\u0063\u0327/\u00c7/", // c + cedille
		code:     http.StatusPermanentRedirect,
		redirect: "/ç/ç/ç/",
	},
	6: {
		route:    "/{nope string}/{label string}/{nonorm int}",
		path:     "/\u0063\u0327/\u0063\u0327/123", // c + cedille
		code:     http.StatusPermanentRedirect,
		redirect: "/\u0063\u0327/ç/123",
	},
	7: {
		route:    "/{label string}/foo",
		path:     "/\u0063\u0327/foo",
		code:     http.StatusPermanentRedirect,
		redirect: "/ç/foo",
	},
}

func TestMiddleware(t *testing.T) {
	for i, tc := range middlewareTestCases {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			normalize := request.Normalize(func(w http.ResponseWriter, _ *http.Request) {
				w.WriteHeader(http.StatusBadRequest)
			}, []string{"project", "username", "org", "label"})
			router := mux.New(
				mux.HandleFunc(http.MethodGet, tc.route, normalize(func(http.ResponseWriter, *http.Request) {
				})),
			)

			req, err := http.NewRequest("GET", tc.path, nil)
			if err != nil {
				t.Fatalf("Error creating new request: %q", err)
			}
			w := httptest.NewRecorder()
			router.ServeHTTP(w, req)
			resp := w.Result()

			if resp.StatusCode != tc.code {
				t.Errorf("Wrong status code: want=%d, got=%d", tc.code, resp.StatusCode)
			}

			location := resp.Header.Get("Location")
			redirectURL, err := url.Parse(tc.redirect)
			if err != nil {
				t.Fatalf("Bad URL: %q", err)
			}
			redirect := strings.ToLower(redirectURL.EscapedPath())
			if location != redirect {
				t.Errorf("Wrong redirect: want=%q, got=%q", redirect, location)
			}
		})
	}
}

var formNormTestCases = [...]struct {
	body   string
	opts   []request.Option
	errs   bool
	badKey string
	final  map[string]string
}{
	0: {},
	1: {
		body: "a=test&b=test2",
		opts: []request.Option{request.Name("a")},
		final: map[string]string{
			"a": "test",
			"b": "test2",
		},
	},
	2: {
		body:   "a=test and&b=test2",
		badKey: "a",
		errs:   true,
		opts:   []request.Option{request.Name("a")},
		final: map[string]string{
			"a": "test and",
			"b": "test2",
		},
	},
	3: {
		body: "broken=%a",
		errs: true,
	},
	4: {
		body: "a=test\r\n\n\rme",
		opts: []request.Option{request.UnixLineEndings("a")},
		final: map[string]string{
			"a": "test\n\n\nme",
		},
	},
	5: {
		body: "a=a\u00A0a\u1680a\u2000a",
		opts: []request.Option{request.OpaqueString("a")},
		final: map[string]string{
			"a": "a a a a",
		},
	},
	6: {
		body:   "a=atom.xml", // `.' is disallowed in names
		opts:   []request.Option{request.Name("a")},
		final:  map[string]string{},
		errs:   true,
		badKey: "a",
	},
	7: {
		body:   "a=%25", // `%' is disallowed in names
		opts:   []request.Option{request.Name("a")},
		final:  map[string]string{},
		errs:   true,
		badKey: "a",
	},
	8: {
		body:   "a=%26", // `&' is disallowed in names
		opts:   []request.Option{request.Name("a")},
		final:  map[string]string{},
		errs:   true,
		badKey: "a",
	},
	9: {
		body:   "a=%3B", // `;' is disallowed in names
		opts:   []request.Option{request.Name("a")},
		final:  map[string]string{},
		errs:   true,
		badKey: "a",
	},
	10: {
		body:   "a=%40", // `@' is disallowed in names
		opts:   []request.Option{request.Name("a")},
		final:  map[string]string{},
		errs:   true,
		badKey: "a",
	},
}

func TestFormNormalization(t *testing.T) {
	for i, tc := range formNormTestCases {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			req := httptest.NewRequest("POST", "/", strings.NewReader(tc.body))
			req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
			badKey, err := request.NormalizeForm(req, tc.opts...)
			if badKey != tc.badKey {
				t.Errorf("Errored on unexpected form key: got=%q, want=%q", badKey, tc.badKey)
			}
			switch {
			case err == nil && tc.errs:
				t.Errorf("Expected form normalization to error")
			case err != nil && !tc.errs:
				t.Errorf("Did not expect an error during form normalization, got: %q", err)
			}

			for k, v := range tc.final {
				vv := req.PostFormValue(k)
				if v != vv {
					t.Errorf("Wrong value for form key %q: got=%q, want=%q", k, vv, v)
				}
			}
		})
	}
}
