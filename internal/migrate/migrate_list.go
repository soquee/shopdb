package migrate

import (
	"context"
	"database/sql"
	"flag"
	"fmt"
	"io/fs"
	"log"
	"os"

	"code.soquee.net/migration"
	"github.com/jackc/pgx/v5/stdlib"
	"mellium.im/cli"
)

func migrateListCmd(
	ctx context.Context,
	migrationsTable string,
	vfs fs.FS,
	pgUser string,
	pgDSN string,
	dryRun bool,
	flags *flag.FlagSet,
	logger, debug *log.Logger,
) *cli.Command {
	return &cli.Command{
		Usage: "ls [options]",
		Description: `List existing migrations and their status.

A * is added after any migrations that have already been run and a ! is added
after any that have been run but don't exist on disk (these will appear after
other migrations and may not be in lexical order). If a database connection
cannot be established or querying for existing migrations fails for any other
reason, "?" is listed after each migration.`,
		Flags: flags,
		Run: func(c *cli.Command, args ...string) (err error) {
			err = flags.Parse(args[1:])
			if err != nil {
				return fmt.Errorf("error parsing flags: %q", err)
			}
			cfg, _, err := ParseConfigLog(pgUser, pgDSN, debug)
			if err != nil {
				return fmt.Errorf("error parsing DSN: %q", err)
			}
			connStr := stdlib.RegisterConnConfig(cfg)
			db, err := sql.Open("pgx", connStr)
			if err != nil {
				return fmt.Errorf("error connecting to db: %w", err)
			}
			defer func() {
				if err := db.Close(); err != nil {
					debug.Printf("error closing database connection: %q", err)
				}
			}()

			return runTx(ctx, dryRun, vfs, db, logger, debug, lsMigrations)
		},
	}
}

func lsMigrations(ctx context.Context, vfs fs.FS, tx *sql.Tx, logger, debug *log.Logger) error {
	walker, err := migration.NewWalker(ctx, migrationsTable, tx)
	if err != nil {
		logger.Printf("error querying existing migrations: %q", err)
	}
	return walker(vfs, func(name string, info fs.DirEntry, status migration.RunStatus) error {
		var modifier string
		switch {
		case info == nil:
			modifier = "!"
		case status == migration.StatusRun:
			modifier = "*"
		case status == migration.StatusUnknown:
			modifier = "?"
		}
		if info != nil {
			// If the migration exists on disk, print the tree name instead of the
			// migrations name as it exists in the database.
			name = info.Name()
		}
		_, err := fmt.Fprintf(os.Stdout, "%s%s\n", name, modifier)
		return err
	})
}
