package migrate

import (
	"context"
	"database/sql"
	"flag"
	"fmt"
	"log"

	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/stdlib"
	"mellium.im/cli"

	"code.soquee.net/migration"
)

func migrateSetupCmd(
	ctx context.Context,
	migrationsTable string,
	pgUser string,
	pgDSN string,
	dryRun bool,
	flags *flag.FlagSet,
	logger, debug *log.Logger,
) *cli.Command {
	return &cli.Command{
		Usage: "setup [options]",
		Description: `Create the database with a migrations table.
		
Creates a database and a migrations table.`,
		Flags: flags,
		Run: func(c *cli.Command, args ...string) (err error) {
			err = flags.Parse(args[1:])
			if err != nil {
				return fmt.Errorf("error parsing flags: %q", err)
			}
			cfg, oldUser, err := ParseConfigLog(pgUser, pgDSN, debug)
			if err != nil {
				return fmt.Errorf("error parsing DSN: %q", err)
			}

			return setupDB(ctx, oldUser, dryRun, cfg, debug)
		},
	}
}

func createDB(ctx context.Context, cfg *pgx.ConnConfig, debug *log.Logger) error {
	// Create a copy of the config so that we don't modify the original. To create
	// the database we want to connect to the cluster, not to any specific DB, so
	// clear the database name from the new config before connecting.
	cfg = cfg.Copy()
	dbName := cfg.Database
	cfg.Database = ""
	connStr := stdlib.RegisterConnConfig(cfg)
	db, err := sql.Open("pgx", connStr)
	if err != nil {
		return err
	}
	defer func() {
		err := db.Close()
		if err != nil {
			debug.Printf("error closing initial database connection: %q", err)
		}
	}()

	_, err = db.ExecContext(ctx, `CREATE DATABASE `+dbName+` WITH ENCODING=UTF8`)
	if err != nil {
		return err
	}
	return nil
}

func setupDB(ctx context.Context, userName string, dryRun bool, cfg *pgx.ConnConfig, debug *log.Logger) (e error) {
	// Create the database to connect to.
	debug.Println(`Creating database "` + cfg.Database + `"…`)
	if !dryRun {
		err := createDB(ctx, cfg, debug)
		if err != nil {
			return err
		}
	}

	var db *sql.DB
	var err error
	if !dryRun {
		connStr := stdlib.RegisterConnConfig(cfg)
		db, err = sql.Open("pgx", connStr)
		if err != nil {
			return err
		}
		defer func() {
			err := db.Close()
			if err != nil {
				debug.Printf("error closing database connection: %q", err)
			}
		}()
	}

	escTable := pgx.Identifier{migrationsTable}.Sanitize()
	escUser := pgx.Identifier{userName}.Sanitize()

	var tx *sql.Tx
	debug.Printf("Starting transaction…")
	if db != nil {
		tx, err = db.BeginTx(ctx, nil)
		if err != nil {
			return err
		}
		defer func() {
			if e != nil {
				/* #nosec */
				_ = tx.Rollback()
			}
		}()
	}
	debug.Printf("Creating migrations table %s if it doesn't already exist…", escTable)
	if tx != nil {
		err = migration.Setup(ctx, migrationsTable, tx)
		if err != nil {
			return err
		}
	}

	debug.Printf("Creating role %s…", escUser)
	if tx != nil {
		_, err = tx.ExecContext(ctx, fmt.Sprintf(`
CREATE ROLE %s
	WITH NOSUPERUSER
	NOCREATEDB
	NOCREATEROLE
	NOINHERIT
	LOGIN
	NOREPLICATION
	NOBYPASSRLS
	CONNECTION LIMIT 100`, escUser))
		if err != nil {
			return err
		}
	}

	debug.Println("Granting privileges for new role…")
	if tx != nil {
		_, err = tx.ExecContext(ctx, fmt.Sprintf(`
ALTER DEFAULT PRIVILEGES
	FOR ROLE current_user
	IN SCHEMA public
	GRANT ALL ON SEQUENCES TO %s`, escUser))
		if err != nil {
			return err
		}
		_, err = tx.ExecContext(ctx, fmt.Sprintf(`
ALTER DEFAULT PRIVILEGES
	FOR ROLE current_user
	IN SCHEMA public
	GRANT ALL ON TABLES TO %s`, escUser))
		if err != nil {
			return err
		}
	}

	debug.Printf("Starting transaction…")
	if tx != nil {
		return tx.Commit()
	}
	return nil
}
