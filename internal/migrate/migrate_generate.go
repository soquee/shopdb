package migrate

import (
	"flag"
	"fmt"
	"log"
	"os"

	"code.soquee.net/migration"
	"mellium.im/cli"
)

func migrateGenCmd(
	logger, debug *log.Logger,
) *cli.Command {
	gen := migration.Generator(baseDir)
	flags := flag.NewFlagSet("gen", flag.ContinueOnError)
	flags.SetOutput(os.Stderr)

	return &cli.Command{
		Usage:       "gen migration_name",
		Description: `Creates a new database migration.`,
		Flags:       flags,
		Run: func(c *cli.Command, args ...string) (err error) {
			switch {
			case len(args) > 2:
				c.Help()
				return fmt.Errorf("too many arguments")
			case len(args) == 0:
				c.Help()
				return fmt.Errorf("missing migration name")
			}
			return gen(args[0])
		},
	}
}
