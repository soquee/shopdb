// The migrate package contains CLI commands for handling migrations.
package migrate

import (
	"context"
	"database/sql"
	"errors"
	"flag"
	"fmt"
	"io"
	"io/fs"
	"log"
	"os"
	"path"

	"code.soquee.net/migration"
	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/tracelog"
	"mellium.im/cli"
)

const (
	baseDir         = "migrations/"
	migrationsTable = "__shopdb_schema_migrations"
)

func ParseConfigLog(pgUser, pgDSN string, debug *log.Logger) (*pgx.ConnConfig, string, error) {
	var oldUser string
	cfg, err := pgx.ParseConfig(pgDSN)
	if cfg != nil {
		oldUser = cfg.User
		if pgUser != "" {
			cfg.User = pgUser
		}
		cfg.Tracer = &tracelog.TraceLog{Logger: tracelog.LoggerFunc(func(_ context.Context, level tracelog.LogLevel, msg string, data map[string]interface{}) {
			debug.Printf("%s: %s %+v", level, msg, data)
		}), LogLevel: tracelog.LogLevelDebug}
	}
	return cfg, oldUser, err
}

// Cmd returns a command that can be used to run, rollback, generate, and list
// database migrations.
func Cmd(
	fs fs.FS,
	logger, debug *log.Logger,
	shutdown <-chan struct{},
) *cli.Command {
	var (
		pgDSN  = os.Getenv("DATABASE_URL")
		pgUser = os.Getenv("SHOPDB_DB_USER")
		dryRun bool
	)
	if pgUser == "" {
		pgUser = "postgres"
	}
	flags := flag.NewFlagSet("migrate", flag.ContinueOnError)
	flags.StringVar(&pgDSN, "db", pgDSN, "A DSN or URI, overrides $DATABASE_URL")
	flags.StringVar(&pgUser, "dbuser", pgUser, "The username to use when running migrations, overrides $SHOPDB_DB_USER")
	flags.BoolVar(&dryRun, "n", dryRun, "Perform a dry run and don't commit any changes, just show what would be done.")

	ctx, cancel := context.WithCancel(context.Background())
	go func() {
		select {
		case <-shutdown:
			cancel()
			return
		case <-ctx.Done():
			return
		}
	}()

	return &cli.Command{
		Usage:       "migrate [options] command",
		Description: `Runs database migrations.`,
		Flags:       flags,
		Commands: []*cli.Command{
			migrateGenCmd(logger, debug),
			migrateSetupCmd(ctx, migrationsTable, pgUser, pgDSN, dryRun, flags, logger, debug),
			migrateListCmd(ctx, migrationsTable, fs, pgUser, pgDSN, dryRun, flags, logger, debug),
			migrateRunCmd(ctx, migrationsTable, fs, pgUser, pgDSN, dryRun, flags, logger, debug),
			migrateRevertCmd(ctx, migrationsTable, fs, pgUser, pgDSN, dryRun, flags, logger, debug),
			migrateRedoCmd(ctx, migrationsTable, fs, pgUser, pgDSN, dryRun, flags, logger, debug),
		},
		Run: func(c *cli.Command, args ...string) error {
			c.Help()
			return nil
		},
	}
}

func runTx(ctx context.Context, dryRun bool, fs fs.FS, db *sql.DB, logger, debug *log.Logger, f func(ctx context.Context, fs fs.FS, tx *sql.Tx, logger, debug *log.Logger) error) (e error) {
	tx, err := db.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	defer func() {
		if e != nil || dryRun {
			err := tx.Rollback()
			if err != nil {
				debug.Printf("error rolling back transaction: %q", err)
			}
			return
		}
		e = tx.Commit()
	}()

	return f(ctx, fs, tx, logger, debug)
}

func runMigrations(ctx context.Context, all bool, vfs fs.FS, tx *sql.Tx, logger, debug *log.Logger) error {
	var ranOne bool
	walker, err := migration.NewWalker(ctx, migrationsTable, tx)
	if walker != nil && err != nil {
		logger.Printf("error querying existing migrations: %q", err)
	}
	err = walker(vfs, func(name string, info fs.DirEntry, status migration.RunStatus) error {
		if ranOne && !all {
			return io.EOF
		}
		if status == migration.StatusUnknown {
			return errors.New("error querying for migrations in the database")
		}
		// Skip migrations that don't exist on disk or have already been run.
		if info == nil || status == migration.StatusRun {
			return nil
		}

		// The migration has not been run; try to run it!
		// If this happens out of order somehow (eg. you insert a migration into the
		// middle) things may break, but I think that's okay for now.
		upPath := path.Join(info.Name(), "up.sql")
		r, err := vfs.Open(upPath)
		if err != nil {
			return err
		}
		defer func() {
			err := r.Close()
			if err != nil {
				debug.Printf("error closing %q: %q", upPath, err)
			}
		}()
		upb, err := io.ReadAll(r)
		if err != nil {
			return err
		}

		logger.Printf("Running migration %q", info.Name())

		_, err = tx.ExecContext(ctx, string(upb))
		if err != nil {
			return err
		}
		_, err = tx.ExecContext(ctx,
			fmt.Sprintf(`INSERT INTO public.%s (version) VALUES ($1)`, pgx.Identifier{migrationsTable}.Sanitize()), name)
		ranOne = true
		return err
	})
	if err != nil && err != io.EOF {
		return err
	}
	return nil
}
