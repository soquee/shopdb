package migrate

import (
	"context"
	"database/sql"
	"flag"
	"fmt"
	"io/fs"
	"log"

	"github.com/jackc/pgx/v5/stdlib"
	"mellium.im/cli"
)

func migrateRedoCmd(
	ctx context.Context,
	migrationsTable string,
	vfs fs.FS,
	pgUser string,
	pgDSN string,
	dryRun bool,
	flags *flag.FlagSet,
	logger, debug *log.Logger,
) *cli.Command {
	return &cli.Command{
		Usage:       "redo [options]",
		Description: `Revert then re-run the last run migration.`,
		Flags:       flags,
		Run: func(c *cli.Command, args ...string) (err error) {
			err = flags.Parse(args[1:])
			if err != nil {
				return fmt.Errorf("error parsing flags: %q", err)
			}
			cfg, _, err := ParseConfigLog(pgUser, pgDSN, debug)
			if err != nil {
				return fmt.Errorf("error parsing DSN: %q", err)
			}
			connStr := stdlib.RegisterConnConfig(cfg)
			db, err := sql.Open("pgx", connStr)
			if err != nil {
				debug.Printf("error connecting to database: %q", err)
			} else {
				defer func() {
					if err := db.Close(); err != nil {
						debug.Printf("error closing database connection: %q", err)
					}
				}()
			}

			return runTx(ctx, dryRun, vfs, db, logger, debug, redoMigration)
		},
	}
}

func redoMigration(ctx context.Context, vfs fs.FS, tx *sql.Tx, logger, debug *log.Logger) error {
	err := revertMigration(ctx, vfs, tx, logger, debug)
	if err != nil {
		return err
	}
	return runMigrations(ctx, false, vfs, tx, logger, debug)
}
