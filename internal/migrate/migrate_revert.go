package migrate

import (
	"context"
	"database/sql"
	"flag"
	"fmt"
	"io"
	"io/fs"
	"log"
	"path"

	"code.soquee.net/migration"
	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/stdlib"
	"mellium.im/cli"
)

func migrateRevertCmd(
	ctx context.Context,
	migrationsTable string,
	vfs fs.FS,
	pgUser string,
	pgDSN string,
	dryRun bool,
	flags *flag.FlagSet,
	logger, debug *log.Logger,
) *cli.Command {
	return &cli.Command{
		Usage:       "revert [options]",
		Description: `Reverts the last run migration.`,
		Flags:       flags,
		Run: func(c *cli.Command, args ...string) (err error) {
			err = flags.Parse(args[1:])
			if err != nil {
				return fmt.Errorf("error parsing flags: %q", err)
			}
			cfg, _, err := ParseConfigLog(pgUser, pgDSN, debug)
			if err != nil {
				return fmt.Errorf("error parsing DSN: %q", err)
			}
			connStr := stdlib.RegisterConnConfig(cfg)
			db, err := sql.Open("pgx", connStr)
			if err != nil {
				debug.Printf("error connecting to database: %q", err)
			} else {
				defer func() {
					if err := db.Close(); err != nil {
						debug.Printf("error closing database connection: %q", err)
					}
				}()
			}

			return runTx(ctx, dryRun, vfs, db, logger, debug, revertMigration)
		},
	}
}

func revertMigration(ctx context.Context, vfs fs.FS, tx *sql.Tx, logger, debug *log.Logger) error {
	version, dir, err := migration.LastRun(ctx, migrationsTable, vfs, tx)
	if err != nil {
		return err
	}

	downPath := path.Join(dir, "down.sql")
	r, err := vfs.Open(downPath)
	if err != nil {
		return err
	}
	defer func() {
		err := r.Close()
		if err != nil {
			debug.Printf("error closing %q: %q", downPath, err)
		}
	}()
	downb, err := io.ReadAll(r)
	if err != nil {
		return err
	}

	logger.Printf("Rolling back migration %q", dir)

	_, err = tx.ExecContext(ctx, string(downb))
	if err != nil {
		return err
	}
	_, err = tx.ExecContext(ctx,
		fmt.Sprintf(`DELETE FROM %s WHERE version=$1`, pgx.Identifier{migrationsTable}.Sanitize()), version)
	return err
}
