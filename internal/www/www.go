// Package www contains servers and templates for the Soquee web UI.
package www

import (
	"context"
	"fmt"
	"io/fs"
	"log"
	"net/http"
	"strconv"
	"strings"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"golang.org/x/text/message/catalog"

	"code.soquee.net/login"
	"code.soquee.net/mux"
	"code.soquee.net/shopdb/internal/store"
	"code.soquee.net/tmpl"
)

type uidmiddleware func(uid int) http.HandlerFunc

// Config contains fields required to create a new endpoint serving the web
// application.
type Config struct {
	Store         *store.DB
	DisableSignup bool
	Version       string
	Namespace     string
	Domain        string
	XSRFKey       string
	SessionKey    string
	Logger        *log.Logger
	Debug         *log.Logger
	C             catalog.Catalog
	Tmpls         tmpl.Template
	Assets        fs.FS
}

// New creates a new handler capable of serving UI requests.
// The handler is rooted at /
// The provided namespace is used on Prometheus metrics in the returned registry
func New(
	ctx context.Context,
	cfg Config,
) (http.Handler, *prometheus.Registry, error) {

	m := newMetrics(cfg.Namespace)

	// Create various middlewares by currying in their dependencies.
	needsLogin := NeedsLogin(cfg.Store.Login, cfg.Domain, cfg.Logger)
	optionLogin := maybeLogin(cfg.Store.Login, cfg.Domain)
	noLogin := NoLogin(cfg.Store.Login, cfg.Domain)
	notFound := optionLogin(errPage(http.StatusNotFound, "", cfg.XSRFKey, cfg.Domain, cfg.Tmpls, cfg.Logger, cfg.Debug))
	//badRequest := optionLogin(errPage(http.StatusBadRequest, "", cfg.XSRFKey, cfg.Domain, cfg.Tmpls, cfg.Logger))
	err500 := optionLogin(errPage(http.StatusInternalServerError, "", cfg.XSRFKey, cfg.Domain, cfg.Tmpls, cfg.Logger, cfg.Debug))
	verifyXSRF := xsrfVerifier(false, cfg.XSRFKey, m.xsrfFailures)
	verifyXSRFHome := xsrfVerifier(true, cfg.XSRFKey, m.xsrfFailures)

	// Static assets
	fileServer := http.StripPrefix("/public", http.FileServer(http.FS(cfg.Assets)))
	handleAssets := publicMIME(func(w http.ResponseWriter, req *http.Request) {
		pathInfo := mux.Param(req, "p")
		path := pathInfo.Raw

		// If they try to query the root directory (eg. /public/) , don't list
		// files.
		// This won't stop file listings from subdirectories but more generic ways
		// to solve this are expensive.
		if pathInfo.Value == nil || path == "" || strings.HasSuffix(path, "/") {
			notFound.ServeHTTP(w, req)
			return
		}
		fileServer.ServeHTTP(w, req)
	})
	// Healthchecks
	var healthCheck http.HandlerFunc = func(w http.ResponseWriter, req *http.Request) {

		h := w.Header()
		h.Add("Shopdb-Version", cfg.Version)

		loginid, uid, _, err := login.GetLoginCookie(w, req, cfg.Domain)
		if err == nil {
			h.Add("Shopdb-UID", strconv.Itoa(uid))
			h.Add("Shopdb-Login", strconv.FormatInt(loginid, 16))
		}

		if req.Method == "GET" {
			fmt.Fprintln(w, "OK")
		}
	}

	// Main page
	//homePage := homePage(cfg.XSRFKey, cfg.Domain, cfg.Tmpls, cfg.Logger)

	// Login
	renderLogin := loginHandler(m.logins, cfg.Store.User, cfg.Store.Login, cfg.Domain, cfg.XSRFKey, cfg.SessionKey, cfg.Tmpls, cfg.Logger, cfg.Debug)

	// Signup
	//signupPage := signupHandler(m.signups, disableSignup, orgStore.InviteStore, cfg.Store.User, cfg.Store.Login, cfg.Domain, cfg.XSRFKey, client, cfg.Tmpls, cfg.Logger, cfg.Debug)

	// Password reset
	//passwordResetPage := resetHandler(cfg.Store.User, cfg.Store.Login, cfg.Domain, cfg.SessionKey, cfg.XSRFKey, client, cfg.Tmpls, cfg.Logger, cfg.Debug)
	//newPasswordPage := newPasswordHandler(cfg.Store.User, cfg.Store.Login, cfg.Domain, cfg.SessionKey, cfg.XSRFKey, client, cfg.Tmpls, cfg.Logger, cfg.Debug)

	serveMux := mux.New(
		mux.NotFound(http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
			w.WriteHeader(http.StatusNotFound)
			notFound(w, req)
		})),
		mux.Handle(http.MethodGet, "/public/{p path}", handleAssets),
		mux.Handle(http.MethodGet, "/.well-known/{p path}", handleAssets),
		mux.Handle(http.MethodHead, "/healthcheck", healthCheck),
		mux.Handle(http.MethodGet, "/healthcheck", healthCheck),

		// Main page
		//mux.Handle(http.MethodGet, "/", needsLogin(homePage)),
		//mux.Handle(http.MethodGet, "/site/privacy", optionLogin(privacyPolicy(cfg.Tmpls, cfg.Logger))),
		//mux.Handle(http.MethodGet, "/site/terms", optionLogin(termsOfService(cfg.Tmpls, cfg.Logger))),
		//mux.Handle(http.MethodGet, "/site/security", optionLogin(securityPolicy(cfg.Tmpls, cfg.Logger))),
		//mux.Handle(http.MethodGet, "/site/contact", optionLogin(contactPage(cfg.Tmpls, cfg.Logger))),

		// Login
		mux.Handle(http.MethodGet, "/login", noLogin(renderLogin)),
		mux.Handle(http.MethodPost, "/login", noLogin(verifyXSRF(renderLogin))),
		mux.Handle(http.MethodPost, "/logout",
			needsLogin(verifyXSRFHome(
				logoutHandler(cfg.Store.Login, cfg.Domain, cfg.Logger, m.logouts),
			)),
		),

		// Signup
		//mux.Handle(http.MethodGet, "/join", noLogin(signupPage)),
		mux.HandleFunc(http.MethodGet, "/register", func(w http.ResponseWriter, r *http.Request) {
			http.Redirect(w, r, "/join", http.StatusTemporaryRedirect)
		}),
		mux.HandleFunc(http.MethodGet, "/signup", func(w http.ResponseWriter, r *http.Request) {
			http.Redirect(w, r, "/join", http.StatusTemporaryRedirect)
		}),
		//mux.Handle(http.MethodPost, "/join", noLogin(verifyXSRF(signupPage))),
		// TODO:
		//mux.Handle(http.MethodGet, "/verify/{uid int}/{token string}", verifyEmail(emailVerifier, cfg.Logger, cfg.Debug)),

		// Password reset
		//mux.Handle(http.MethodGet, "/password_reset", noLogin(passwordResetPage)),
		//mux.Handle(http.MethodPost, "/password_reset", noLogin(verifyXSRF(passwordResetPage))),
		//mux.Handle(http.MethodGet, "/password_reset/{uid int}/{token string}", noLogin(newPasswordPage)),
		//mux.Handle(http.MethodPost, "/password_reset/{uid int}/{token string}", noLogin(verifyXSRF(newPasswordPage))),

		// Avatars
		//mux.Handle(http.MethodGet, "/avatars/*", avatarHandler()),
	)

	var h http.Handler = securityHeaders(cfg.Domain, panicCatcher(
		serveMux,
		m.panics,
		err500,
		cfg.Debug,
	))
	h = promhttp.InstrumentHandlerCounter(m.requests, h)
	h = promhttp.InstrumentHandlerInFlight(m.requestsInFlight, h)
	h = promhttp.InstrumentHandlerTimeToWriteHeader(m.timeToResponseHeaders, h)
	h = promhttp.InstrumentHandlerDuration(m.requestDuration, h)

	return h, m.Registry, nil
}
