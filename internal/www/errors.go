package www

import (
	"log"
	"net/http"

	"code.soquee.net/tmpl"
)

// internalServerErrorRenderer returns a function that renders a 500 error page.
func internalServerErrorRenderer(
	domain,
	xsrfKey string,
	tmpls tmpl.Template,
	logger, debug *log.Logger,
) func(uid int, w http.ResponseWriter, r *http.Request) {
	render := renderer(domain, xsrfKey, "InternalServerError", "Internal Server Error", tmpls, logger, nil)
	return func(uid int, w http.ResponseWriter, r *http.Request) {
		err := render(uid, tmpl.Flash{}, w, r, nil)
		if err != nil {
			debug.Printf("error rendering internal server error: %v", err)
		}
	}
}

type errorData struct {
	Code  int
	Title string
}

func errPage(code int, msg, xsrfKey, domain string, tmpls tmpl.Template, logger, debug *log.Logger) uidmiddleware {
	render500 := internalServerErrorRenderer(domain, xsrfKey, tmpls, logger, debug)
	render := renderer(domain, xsrfKey, "errs", "Error", tmpls, logger, nil)
	if msg == "" {
		msg = http.StatusText(code)
	}

	return func(uid int) http.HandlerFunc {
		return func(w http.ResponseWriter, r *http.Request) {
			if code == 500 {
				render500(uid, w, r)
				return
			}

			err := render(uid, tmpl.Flash{}, w, r, errorData{
				Code:  code,
				Title: msg,
			})
			if err != nil {
				debug.Printf("error rendering error page: %v", err)
			}
		}
	}
}
