package www

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

var ctTests = map[string]string{
	"/test.js":           "application/javascript; charset=utf-8",
	"/test.css":          "text/css; charset=utf-8",
	"/whatever/test.css": "text/css; charset=utf-8",
	"/test.json":         "application/json; charset=utf-8",
	"/test.svg":          "image/svg+xml; charset=utf-8",
	"/test.svgz":         "image/svg+xml; charset=utf-8",
	"/test.whatever":     "Content-Type: application/octet-stream",
	"/whatever":          "Content-Type: application/octet-stream",
}

func TestContentType(t *testing.T) {
	for fname, ct := range ctTests {
		var innerRun bool
		h := publicMIME(func(w http.ResponseWriter, r *http.Request) {
			innerRun = true
		})
		r := httptest.NewRequest("GET", fname, nil)
		w := httptest.NewRecorder()
		h.ServeHTTP(w, r)

		if !innerRun {
			t.Errorf("expected next handler to be run")
		}
		resp := w.Result()
		if resp == nil {
			t.Fatalf("unexpected nil response")
		}
		foundCT := resp.Header.Get("Content-Type")
		if foundCT != ct {
			t.Errorf("unexpected content type for file %q: want=%q, got=%q", fname, ct, foundCT)
		}
	}
}
