package www

import (
	"log"
	"net/http"

	"github.com/prometheus/client_golang/prometheus"
)

const (
	// Subsystem is the subsystem part of the name of all Prometheus metrics
	// generated in this package.
	Subsystem = "www"
)

func panicCatcher(
	handler http.Handler,
	c prometheus.Counter,
	err500 http.Handler,
	debug *log.Logger,
) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		defer func() {
			if r := recover(); r != nil {
				debug.Println(r)
				err500.ServeHTTP(w, req)
				c.Inc()
			}
		}()
		handler.ServeHTTP(w, req)
	}
}

type metrics struct {
	*prometheus.Registry

	panics                prometheus.Counter
	signups               *prometheus.CounterVec
	accountDels           *prometheus.CounterVec
	requests              *prometheus.CounterVec
	requestsInFlight      prometheus.Gauge
	timeToResponseHeaders *prometheus.HistogramVec
	requestDuration       *prometheus.HistogramVec
	logouts               *prometheus.CounterVec
	logins                *prometheus.CounterVec
	xsrfFailures          *prometheus.CounterVec
	webhooks              *prometheus.CounterVec
}

func newMetrics(namespace string) metrics {
	m := metrics{
		Registry: prometheus.NewPedanticRegistry(),

		signups: prometheus.NewCounterVec(prometheus.CounterOpts{
			Namespace: namespace,
			Subsystem: Subsystem,
			Name:      "signups",
			Help:      "Counts the number of new account signups.",
		}, []string{"failed"}),
		accountDels: prometheus.NewCounterVec(prometheus.CounterOpts{
			Namespace: namespace,
			Subsystem: Subsystem,
			Name:      "account_deletions",
			Help:      "Counts the number of times a user deletes their account.",
		}, []string{"failed"}),
		panics: prometheus.NewCounter(prometheus.CounterOpts{
			Namespace: namespace,
			Subsystem: Subsystem,
			Name:      "panics",
			Help:      "Counts the number of WWW panics.",
		}),
		requests: prometheus.NewCounterVec(prometheus.CounterOpts{
			Namespace: namespace,
			Subsystem: Subsystem,
			Name:      "requests",
			Help:      "Counts the number of WWW requests.",
		}, []string{"code", "method"}),
		requestsInFlight: prometheus.NewGauge(prometheus.GaugeOpts{
			Namespace: namespace,
			Subsystem: Subsystem,
			Name:      "requests_in_flight",
			Help:      "Counts the number of currently active requests.",
		}),
		timeToResponseHeaders: prometheus.NewHistogramVec(prometheus.HistogramOpts{
			Namespace: namespace,
			Subsystem: Subsystem,
			Name:      "time_to_response_headers",
			Help:      "Counts the duration until the response headers have been written .",
			Buckets:   []float64{0.1, 0.3, 1.2},
		}, []string{"code", "method"}),
		requestDuration: prometheus.NewHistogramVec(prometheus.HistogramOpts{
			Namespace: namespace,
			Subsystem: Subsystem,
			Name:      "request_duration",
			Help:      "Counts the duration of WWW requests.",
			Buckets:   []float64{0.1, 0.3, 1.2},
		}, []string{"code", "method"}),
		logouts: prometheus.NewCounterVec(prometheus.CounterOpts{
			Namespace: namespace,
			Subsystem: Subsystem,
			Name:      "logouts",
			Help:      "The number of logouts.",
		}, []string{"path", "failed"}),
		logins: prometheus.NewCounterVec(prometheus.CounterOpts{
			Namespace: namespace,
			Subsystem: Subsystem,
			Name:      "logins",
			Help:      "The number of logins.",
		}, []string{"failed"}),
		xsrfFailures: prometheus.NewCounterVec(prometheus.CounterOpts{
			Namespace: namespace,
			Subsystem: Subsystem,
			Name:      "xsrf_failures",
			Help:      "The number of failed XSRF token verifications.",
		}, []string{
			"path",
		}),
		webhooks: prometheus.NewCounterVec(prometheus.CounterOpts{
			Namespace: namespace,
			Subsystem: Subsystem,
			Name:      "webhooks",
			Help:      "The number of processed webhook events and their status.",
		}, []string{
			"livemode",
			"type",
			"failed",
		}),
	}
	m.MustRegister(
		m.panics,
		m.signups,
		m.accountDels,
		m.requests,
		m.requestsInFlight,
		m.timeToResponseHeaders,
		m.requestDuration,
		m.logouts,
		m.logins,
		m.xsrfFailures,
		m.webhooks,
	)

	return m
}
