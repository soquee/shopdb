package www

import (
	"io"
	"log"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"code.soquee.net/testlog"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

// Counter is a dummy prometheus.Counter that records any calls to add or Inc
// in Count.
type Counter struct {
	prometheus.Metric
	prometheus.Collector

	Count float64
}

// Inc implements prometheus.Counter for Counter.
func (c *Counter) Inc() {
	c.Count++
}

// Add implements prometheus.Counter for Counter.
func (c *Counter) Add(f float64) {
	c.Count += f
}

func TestPanicCatcher(t *testing.T) {
	c := &Counter{}
	w := httptest.NewRecorder()

	h := http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		panic("Should be caught")
	})
	const code500 = 123
	err500 := http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		w.WriteHeader(code500)
	})
	h = panicCatcher(h, c, err500, log.New(io.Discard, "", 0))

	h.ServeHTTP(w, httptest.NewRequest("GET", "/", nil))

	if c.Count != 1 {
		t.Errorf("Expected panic catcher to increment the counter: want=1.0, got=%f", c.Count)
	}
	if code := w.Result().StatusCode; code != code500 {
		t.Errorf("Wrong result from 500 error handler: want=%d, got=%d", code500, code)
	}
}

func TestNewMetrics(t *testing.T) {
	const namespace = "testing"
	logger := testlog.New(t)

	m := newMetrics(namespace)
	m.logouts.With(prometheus.Labels{
		"path":   "/test",
		"failed": "true",
	}).Inc()

	regHandler := promhttp.HandlerFor(m.Registry, promhttp.HandlerOpts{
		ErrorLog:           logger,
		DisableCompression: true,
	})
	recorder := httptest.NewRecorder()
	regHandler.ServeHTTP(recorder, httptest.NewRequest("GET", "/", nil))

	// Check that the metrics contain various names.
	s := recorder.Body.String()
	for _, v := range []string{"panics", "logouts"} {
		fqname := namespace + "_" + Subsystem + "_" + v
		if !strings.Contains(s, fqname) {
			t.Errorf("Expected metrics to contain %q", fqname)
		}
	}
}
