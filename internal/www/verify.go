package www

import (
	"net/http"
	"strconv"

	"code.soquee.net/tmpl"
	"github.com/prometheus/client_golang/prometheus"
	"golang.org/x/net/xsrftoken"
)

// xsrfVerifier returns a middleware that checks for a post form value of "xsrf"
// and attempts to verify that it is valid.
func xsrfVerifier(redirectHome bool, xsrfKey string, failures *prometheus.CounterVec) func(h uidmiddleware) uidmiddleware {
	return func(h uidmiddleware) uidmiddleware {
		return func(uid int) http.HandlerFunc {
			return func(w http.ResponseWriter, r *http.Request) {
				path := r.URL.Path
				// Special case for posting to the logout form which can be done from the
				// navbar on any page. Maybe there's a better way to do this if we stop
				// auto-injecting the path when generating XSRF tokens.
				if r.PostFormValue("action") == "logout" {
					path = r.PostFormValue("path")
				}
				valid := xsrftoken.Valid(r.PostFormValue("xsrf"), xsrfKey, strconv.Itoa(uid), path)
				if !valid {
					setFlash(w, tmpl.FlashDanger, "Request expired")
					// If the mux we're operating on has a GET handler for the current
					// path, redirect back there. If not go back home on error.
					if redirectHome {
						http.Redirect(w, r, "/", http.StatusFound)
						return
					}
					http.Redirect(w, r, r.URL.Path, http.StatusFound)
					return
				}

				h(uid).ServeHTTP(w, r)
			}
		}
	}
}
