package www

import (
	"log"
	"net/http"

	"code.soquee.net/tmpl"
)

func renderer(domain, xsrfKey, tmplName, title string, tmpls tmpl.Template, logger *log.Logger, data func(tmpl.Page) interface{}) tmpl.RenderFunc {
	render := tmpl.Renderer(domain, xsrfKey, tmplName, tmpls, data)

	return func(uid int, flash tmpl.Flash, w http.ResponseWriter, r *http.Request, extraData interface{}) error {
		err := render(uid, flash, w, r, extraData)
		if err != nil {
			logger.Printf("Error rendering template %q: %q", tmplName, err)
		}
		return nil
	}
}

func setFlash(w http.ResponseWriter, flashType tmpl.FlashType, msg string) {
	tmpl.SetFlash(w, tmpl.Flash{Type: flashType, Message: msg})
}
