package www

import (
	"bytes"
	/* #nosec */
	"crypto/sha1"
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"golang.org/x/net/xsrftoken"

	"code.soquee.net/login"
	"code.soquee.net/norm"
	"code.soquee.net/otp"
	"code.soquee.net/shopdb/internal/store"
	"code.soquee.net/tmpl"
)

// This file handles functions related to user credentials and is considered
// dangerous to touch. Be careful with your code review in this file (even if
// you can't see how the change would cause a security issue).
//
// For more information and recommendations see the package level documentation
// for code.soquee.net/login.

const (
	noUID     = -1
	tokenLen  = 6
	tokenTime = 30 * time.Second
)

// maybeLogin returns a middleware that checks for a login cookie and if it
// finds one passes the valid UID to the next handler.
// If it does not find a login cookie (or the login cookie is invalid), it
// passes in noUID for the UID.
func maybeLogin(store store.Login, domain string) func(uidmiddleware) http.HandlerFunc {
	return func(f uidmiddleware) http.HandlerFunc {
		return func(w http.ResponseWriter, r *http.Request) {
			loginID, uid, tok, err := login.GetLoginCookie(w, r, domain)
			if err != nil {
				f(noUID).ServeHTTP(w, r)
				return
			}
			valid, err := store.CheckLogin(r.Context(), loginID, uid, tok, r)
			if err != nil || !valid {
				f(noUID).ServeHTTP(w, r)
				return
			}

			f(uid).ServeHTTP(w, r)
		}
	}
}

// NoLogin returns a middleware that checks for a valid login cookie.
// If a valid login cookie is found, we are redirected home.
// Otherwise the next middleware is called.
func NoLogin(store store.Login, domain string) func(uidmiddleware) http.HandlerFunc {
	return func(h uidmiddleware) http.HandlerFunc {
		return func(w http.ResponseWriter, r *http.Request) {
			loginID, uid, tok, err := login.GetLoginCookie(w, r, domain)
			if err != nil {
				h(noUID).ServeHTTP(w, r)
				return
			}
			valid, err := store.CheckLogin(r.Context(), loginID, uid, tok, r)
			if err != nil || !valid {
				h(noUID).ServeHTTP(w, r)
				return
			}

			http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		}
	}
}

// NeedsLogin returns a middleware that checks for a valid login cookie.
// If one is found, the next handler is called.
// If not a temporary redirect to redirectURL is served.
func NeedsLogin(store store.Login, domain string, logger *log.Logger) func(uidmiddleware) http.HandlerFunc {
	return func(f uidmiddleware) http.HandlerFunc {
		return func(w http.ResponseWriter, r *http.Request) {
			loginID, uid, tok, err := login.GetLoginCookie(w, r, domain)
			if err != nil {
				if err != login.ErrInvalidCookie && err != http.ErrNoCookie {
					logger.Printf("login: error getting login cookie: %q", err)
				}
				http.Redirect(w, r, "/login", http.StatusTemporaryRedirect)
				return
			}
			valid, err := store.CheckLogin(r.Context(), loginID, uid, tok, r)
			if err != nil {
				logger.Printf("login: error checking login: %q", err)
				http.Redirect(w, r, "/login", http.StatusTemporaryRedirect)
				return
			}
			if valid {
				f(uid).ServeHTTP(w, r)
				return
			}

			// Login expired
			http.Redirect(w, r, "/login", http.StatusTemporaryRedirect)
		}
	}
}

func logoutHandler(loginStore store.Login, domain string, logger *log.Logger, logouts *prometheus.CounterVec) func(uid int) http.HandlerFunc {
	return func(uid int) http.HandlerFunc {
		return func(w http.ResponseWriter, r *http.Request) {
			err := r.ParseForm()
			if err != nil {
				logger.Printf("Error parsing logout form: %q", err)
				logouts.With(prometheus.Labels{
					"path":   "errbadform",
					"failed": "true",
				}).Inc()
				http.Redirect(w, r, "/", http.StatusFound)
				return
			}

			loginID, _, _, err := login.GetLoginCookie(w, r, domain)
			if err != nil {
				// This should be impossible because we should always be wrapped in
				// NeedsLogin which has already validated the cookie.
				// TODO: Should that just be baked into this handler?
				logger.Printf("logout handler called when not logged in: %q", err)
				logouts.With(prometheus.Labels{
					"path":   r.PostFormValue("path"),
					"failed": "true",
				}).Inc()
				http.Redirect(w, r, "/", http.StatusFound)
				return
			}
			login.ExpireLoginCookie(w, domain)
			err = loginStore.Logout(r.Context(), uid, loginID)
			if err != nil {
				logger.Printf("error removing login from database: %q", err)
				logouts.With(prometheus.Labels{
					"path":   r.PostFormValue("path"),
					"failed": "true",
				}).Inc()
				http.Redirect(w, r, "/login", http.StatusFound)
				return
			}

			logouts.With(prometheus.Labels{
				"path":   r.PostFormValue("path"),
				"failed": "false",
			}).Inc()
			http.Redirect(w, r, "/login", http.StatusFound)
		}
	}
}

func loginHandler(logins *prometheus.CounterVec, userStore store.User, loginStore store.Login, domain, xsrfKey, sessionKey string, tmpls tmpl.Template, logger, debug *log.Logger) uidmiddleware {
	render := renderer(domain, xsrfKey, "login.tmpl", "Login", tmpls, logger, nil)
	renderTwoFactor := renderer(domain, xsrfKey, "login_two.tmpl", "Multi-Factor Auth", tmpls, logger, nil)

	return func(_ int) http.HandlerFunc {
		return func(w http.ResponseWriter, r *http.Request) {
			invite := r.URL.Query().Get("invite")
			if r.Method == "GET" {
				err := render(noUID, tmpl.Flash{}, w, r, invite)
				if err != nil {
					debug.Printf("error rendering login page: %v", err)
				}
				return
			}

			var multiStage bool
			defer func() {
				if multiStage {
					return
				}

				logins.With(prometheus.Labels{
					// If no login cookie was set, signup failed.
					"failed": strconv.FormatBool(w.Header().Get("Set-Cookie") == ""),
				}).Inc()
			}()

			ctx := r.Context()
			err := r.ParseForm()
			if err != nil {
				logger.Printf("error parsing login form: %q", err)
				w.WriteHeader(http.StatusBadRequest)
				err = render(noUID, tmpl.Flash{
					Type:    tmpl.FlashDanger,
					Message: "Error processing login",
				}, w, r, invite)
				if err != nil {
					debug.Printf("error rendering login processing error: %v", err)
				}
				return
			}

			const (
				emailField = "user_email"
				passField  = "user_password"
			)

			field, err := norm.NormalizeForm(r,
				norm.Email(emailField),
				norm.Password(passField),
			)
			if err != nil {
				w.WriteHeader(http.StatusBadRequest)
				msg := "Form failed validation"
				switch field {
				case emailField:
					msg = "Invalid email entered"
				case passField:
					msg = "Invalid password entered"
				}
				err = render(noUID, tmpl.Flash{
					Type:    tmpl.FlashDanger,
					Message: msg,
				}, w, r, invite)
				if err != nil {
					debug.Printf("error rendering form verification failure: %v", err)
				}
				return
			}
			email := r.PostFormValue(emailField)
			uid, salt, passhash, scheme, twofactor, err := userStore.FetchPassHash(ctx, email)
			if err != nil {
				if err != sql.ErrNoRows {
					logger.Printf("error getting password hash from database: %q", err)
				}
				w.WriteHeader(http.StatusUnauthorized)
				err = render(noUID, tmpl.Flash{
					Type:    tmpl.FlashDanger,
					Message: "Invalid email or password",
				}, w, r, invite)
				if err != nil {
					debug.Printf("error rendering invalid password: %v", err)
				}
				return
			}

			// This is a multi-factor auth code submission.
			loginToken := r.PostFormValue("login_token")
			if loginToken != "" {
				user := r.PostFormValue("totp_user")
				/* #nosec */
				uid64, _ := strconv.ParseInt(user, 10, 32)
				uid := int(uid64)
				// Validate the login token which asserts that the user had a successful
				// password-based auth.
				tokenSecret := fmt.Sprintf("%s_%t_%x", sessionKey, twofactor, passhash)
				valid := xsrftoken.Valid(loginToken, tokenSecret, user, r.URL.Path)
				if !valid {
					w.WriteHeader(http.StatusBadRequest)
					err = render(noUID, tmpl.Flash{
						Type:    tmpl.FlashDanger,
						Message: "Login expired",
					}, w, r, invite)
					if err != nil {
						debug.Printf("error rendering expired login msg: %v", err)
					}
					return
				}

				_, _, _, secret, err := userStore.GetTOTP(ctx, uid)
				if err != nil {
					logger.Printf("Error fetching two-factor secret: %q", err)
					w.WriteHeader(http.StatusInternalServerError)
					err = render(noUID, tmpl.Flash{
						Type:    tmpl.FlashDanger,
						Message: "Error logging in",
					}, w, r, invite)
					if err != nil {
						debug.Printf("error redering two factor fetch error: %v", err)
					}
					return
				}
				gen := otp.NewOTP(secret, tokenLen, sha1.New, otp.TOTP(tokenTime, time.Now))

				/* #nosec */
				token, _ := strconv.ParseInt(r.PostFormValue("totp_token"), 10, 32)
				// First try the current token, then try a window on either side of the
				// token to try and avoid breakage due to minor desynced clocks.
				if t := gen(0, nil); int32(token) != t {
					if t := gen(noUID, nil); int32(token) != t {
						if t := gen(1, nil); int32(token) != t {
							w.WriteHeader(http.StatusBadRequest)
							err = renderTwoFactor(noUID, tmpl.Flash{
								Type:    tmpl.FlashDanger,
								Message: "Invalid token",
							}, w, r, struct {
								User       int
								LoginToken string
							}{
								User:       uid,
								LoginToken: loginToken,
							})
							if err != nil {
								debug.Printf("error rendering two factor tmpl: %v", err)
							}
							multiStage = true
							return
						}
					}
				}

				// Cleanup expired logins.
				err = loginStore.ClearBefore(ctx, nil, uid, time.Now().Add(-1*login.LoginCookieDuration))
				if err != nil {
					// This isn't critical.
					debug.Printf("Error clearing expired logins for uid-%d: %q", uid, err)
				}

				// Issue the user with a signed login cookie.
				loginID, tok, err := loginStore.NewLogin(ctx, uid, r)
				if err != nil {
					logger.Printf("Error creating new login for uid-%d: %q", uid, err)
					w.WriteHeader(http.StatusInternalServerError)
					err = render(noUID, tmpl.Flash{
						Type:    tmpl.FlashDanger,
						Message: "Error logging in",
					}, w, r, invite)
					if err != nil {
						debug.Printf("error rendering login error: %v", err)
					}
					return
				}
				login.SetLoginCookie(w, domain, tok, loginID, uid)

				if invite == "" {
					http.Redirect(w, r, "/", http.StatusFound)
					return
				}
				http.Redirect(w, r, fmt.Sprintf("/organizations/invite?token=%s", invite), http.StatusFound)
				return
			}

			_, err = norm.NormalizeForm(r,
				norm.Password(passField))
			switch err {
			case norm.ErrLongPassword:
				w.WriteHeader(http.StatusUnauthorized)
				err = render(noUID, tmpl.Flash{
					Type:    tmpl.FlashDanger,
					Message: fmt.Sprintf("Password is too long, keep it under %d characters", norm.MaxPasswordLength),
				}, w, r, invite)
				if err != nil {
					debug.Printf("error rendering password too long message: %v", err)
				}
				return
			case norm.ErrShortPassword:
				w.WriteHeader(http.StatusUnauthorized)
				err = render(noUID, tmpl.Flash{
					Type:    tmpl.FlashDanger,
					Message: fmt.Sprintf("Password is too short, use at least %d characters", norm.MinPasswordLength),
				}, w, r, invite)
				if err != nil {
					debug.Printf("error rendering password too short message: %v", err)
				}
				return
			case nil:
			default:
				setFlash(w, tmpl.FlashDanger, "Password contained invalid characters")
				if invite == "" {
					http.Redirect(w, r, "/login", http.StatusFound)
					return
				}
				http.Redirect(w, r, fmt.Sprintf("/login?invite=%s", invite), http.StatusFound)
				return
			}

			credential := []byte(r.PostFormValue(passField))

			h := login.Protect(scheme, credential, salt)
			if !bytes.Equal(h, passhash) {
				w.WriteHeader(http.StatusUnauthorized)
				err = render(noUID, tmpl.Flash{
					Type:    tmpl.FlashDanger,
					Message: "Invalid email or password",
				}, w, r, invite)
				if err != nil {
					debug.Printf("error rendering invalid email template: %v", err)
				}
				return
			}

			// If the user needs to upgrade their password storage mechanism, do so
			// once they have a successful login.
			err = userStore.UpgradeScheme(ctx, salt, h, scheme, email)
			if err != nil {
				logger.Printf("Error upgrading password: %q", err)
			}

			if twofactor {
				tokenSecret := fmt.Sprintf("%s_%t_%x", sessionKey, true, passhash)
				err = renderTwoFactor(noUID, tmpl.Flash{}, w, r, struct {
					User       int
					UserEmail  string
					LoginToken string
				}{
					User:       uid,
					UserEmail:  email,
					LoginToken: xsrftoken.Generate(tokenSecret, strconv.Itoa(uid), r.URL.Path),
				})
				if err != nil {
					debug.Printf("error rendering two factor template: %v", err)
				}
				return
			}

			// Cleanup expired logins.
			err = loginStore.ClearBefore(ctx, nil, uid, time.Now().Add(-1*login.LoginCookieDuration))
			if err != nil {
				// This isn't critical.
				debug.Printf("error clearing expired logins for uid-%d: %q", uid, err)
			}

			// Issue the user with a signed login cookie.
			loginID, tok, err := loginStore.NewLogin(ctx, uid, r)
			if err != nil {
				logger.Printf("error creating new login for uid %d: %q", uid, err)
				w.WriteHeader(http.StatusInternalServerError)
				err = render(noUID, tmpl.Flash{
					Type:    tmpl.FlashDanger,
					Message: "Error logging in",
				}, w, r, invite)
				if err != nil {
					debug.Printf("error rendering error message: %v", err)
				}
				return
			}
			login.SetLoginCookie(w, domain, tok, loginID, uid)

			if invite == "" {
				http.Redirect(w, r, "/", http.StatusFound)
				return
			}
			http.Redirect(w, r, fmt.Sprintf("/organizations/invite?token=%s", invite), http.StatusFound)
		}
	}
}
