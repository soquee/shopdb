//go:build tools
// +build tools

//go:generate go run github.com/99designs/gqlgen generate
//go:generate go run -tags=tools golang.org/x/text/cmd/gotext update -out catalog.go

package tools

import (
	_ "github.com/99designs/gqlgen"
	_ "github.com/99designs/gqlgen/graphql/introspection"
	_ "golang.org/x/text/cmd/gotext"
)
