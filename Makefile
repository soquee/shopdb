.POSIX:
.SUFFIXES:

# make(1) from NetBSD uses .CURDIR, but GNU Make uses CURDIR.
# In case some other version of Make doesn't support either, fallback to $PWD.
.CURDIR ?= $(CURDIR)
.CURDIR ?= $(PWD)

GOFILES!=find . -name '*.go'
LOCALEFILES!=find ./locales -name '*.json'
MIGRATIONS!=find ./migrations -name '*.sql'

GO=go
TAGS=
VERSION!=git describe --dirty

GOLDFLAGS =-s -w
GOLDFLAGS+=-X main.Version=$(VERSION)
GOLDFLAGS+=-extldflags $(LDFLAGS)
GCFLAGS   =
ASMFLAGS  =

shopdb: go.mod internal/graph/generated.go $(GOFILES) $(MIGRATIONS)
	$(GO) build \
		-trimpath \
		-gcflags="$(GCFLAGS)" \
		-asmflags="$(ASMFLAGS)" \
		-tags "$(TAGS)" \
		-o $@ \
		-ldflags "$(GOLDFLAGS)"

catalog.go: $(LOCALEFILES) tools.go
	$(GO) generate -run="gotext" tools.go

internal/graph/generated.go: internal/graph/schema.resolvers.go internal/graph/schema.graphqls internal/graph/resolver.go gqlgen.yml tools.go
	$(GO) generate -run="gqlgen" tools.go

.PHONY: serve
serve:
	$(GO) run \
		-trimpath \
		-gcflags="$(GCFLAGS)" \
		-asmflags="$(ASMFLAGS)" \
		-tags "$(TAGS)" \
		-ldflags "$(GOLDFLAGS)" \
		. -v serve

CONTRIBUTORS: FORCE
	echo "// This is the official list of contributors for copyright purposes." > $@
	echo "//" >> $@
	echo "// If you see your name twice, please fix your commits or create a .mailmap" >> $@
	echo "// entry for yourself and regenerate this file by running make CONTRIBUTORS." >> $@
	echo "// For more info see https://www.git-scm.com/docs/git-check-mailmap" >> $@
	echo "" >> $@
	git --no-pager shortlog --summary --email HEAD | cut -f2- >> $@

FORCE:
