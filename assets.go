//go:build !dev
// +build !dev

package main

import (
	"embed"
	"io/fs"

	"golang.org/x/text/message/catalog"

	"code.soquee.net/tmpl"
)

//go:embed assets
var assets embed.FS

func getTmpls(c catalog.Catalog) (tmpl.Template, fs.FS, error) {
	publicFS, err := fs.Sub(assets, "assets/public")
	if err != nil {
		return tmpl.Template{}, nil, err
	}
	tmplFS, err := fs.Sub(assets, "assets/pages")
	if err != nil {
		return tmpl.Template{}, nil, err
	}
	baseTmplFS, err := fs.Sub(assets, "assets/layouts")
	if err != nil {
		return tmpl.Template{}, nil, err
	}
	tmpls, err := tmpl.New(
		tmpl.FS(tmplFS),
		tmpl.BaseFS(baseTmplFS),
		tmpl.Catalog(c),
	)
	return tmpls, publicFS, err
}
