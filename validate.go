package main

import (
	"bufio"
	"log"
	"os"

	"mellium.im/cli"

	"code.soquee.net/norm"
)

// validateCmd returns a command that can be used to validate usernames.
func validateCmd(logger *log.Logger) *cli.Command {
	return &cli.Command{
		Usage: "validate [name1 name2 …]",
		Description: `Tool for validating user and organization names.

If no names are provided, a list of newline separated names is read from stdin.
Validate only checks if the names are valid, it does not check if they already
exist or connect to the database.`,
		Run: func(c *cli.Command, args ...string) error {
			iterArgsOrStdin(logger, func(arg string) {
				o, err := norm.URLProfile.String(arg)
				if err != nil {
					logger.Printf("Name %q is invalid with error: %v\n", arg, err)
					return
				}
				if o == arg {
					logger.Printf("Name %q is valid and canonical.\n", arg)
					return
				}
				logger.Printf("Name %q is valid, canonical version is %q.", arg, o)
			}, args...)
			return nil
		},
	}
}

func iterArgsOrStdin(logger *log.Logger, f func(arg string), args ...string) {
	if len(args) > 0 {
		for _, arg := range args {
			f(arg)
		}
	} else {
		scanner := bufio.NewScanner(os.Stdin)
		for scanner.Scan() {
			f(scanner.Text())
		}
		if err := scanner.Err(); err != nil {
			logger.Println(err)
		}
	}
}
