package main

import (
	"context"
	"crypto/tls"
	"database/sql"
	"flag"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"time"

	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/playground"
	"golang.org/x/net/netutil"
	"golang.org/x/text/message"
	"mellium.im/cli"

	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/stdlib"
	"github.com/jackc/pgx/v5/tracelog"

	"code.soquee.net/shopdb/internal/graph"
	"code.soquee.net/shopdb/internal/store"
	"code.soquee.net/shopdb/internal/www"
)

const (
	envXSRF    = "SHOPDB_XSRF_SECRET"
	envSession = "SHOPDB_SESSION_SECRET"
	envDomain  = "SHOPDB_DOMAIN"
)

const (
	// Namespace used for metrics in this application.
	Namespace = "shopdb"

	// maxGraphAccept is the maximum number of GraphQL requests that this machine
	// will process concurrently.
	maxGraphAccept = 1024

	// maxWWWAccept is the maximum number of web requests that this machine will
	// process concurrently.
	maxWWWAccept = 1024
)

// serveCmd returns a command that starts an HTTP server that serves debug info.
func serveCmd(
	version string,
	logger, debug *log.Logger,
	shutdown <-chan struct{},
) *cli.Command {
	var (
		debugListen  = getEnvOrDef("SHOPDB_DEBUG_LISTEN", "127.0.0.1:8008")
		graphListen  = getEnvOrDef("SHOPDB_GRAPH_LISTEN", "127.0.0.1:8080")
		usepprof     = getEnvBool("SHOPDB_PPROF")
		readTimeout  = 10 * time.Second
		writeTimeout = 30 * time.Second
		pgDSN        = os.Getenv("DATABASE_URL")
		xsrfKey      = os.Getenv(envXSRF)
		sessionKey   = os.Getenv(envSession)
		domain       = os.Getenv(envDomain)

		// WWW Config
		wwwListen = getEnvOrDef("SHOPDB_WWW_LISTEN", "127.0.0.1:8081")
		wwwCrt    = os.Getenv("SHOPDB_WWW_CRT")
		wwwKey    = os.Getenv("SHOPDB_WWW_KEY")
	)
	flags := flag.NewFlagSet("serve", flag.ContinueOnError)
	flags.StringVar(&debugListen, "debug", debugListen, "An address to listen on for debug info and metrics")
	flags.StringVar(&graphListen, "graph", graphListen, "An address to listen on for graphql queries")
	flags.StringVar(&xsrfKey, "xsrf", xsrfKey, "A secret used to sign XSRF tokens, overrides $"+envXSRF)
	flags.StringVar(&sessionKey, "session", sessionKey, "A secret used to sign session cookies, overrides $"+envSession)
	flags.StringVar(&domain, "domain", domain, "The domain where the application will be served, overrides $"+envDomain)
	flags.DurationVar(&readTimeout, "maxread", readTimeout, "The max read duration for request headers")
	flags.DurationVar(&writeTimeout, "maxwrite", writeTimeout, "The max duration of writes")
	flags.StringVar(&pgDSN, "db", pgDSN, "A postgres URL or DSN, overrides $DATABASE_URL")
	flags.BoolVar(&usepprof, "pprof", usepprof, "Enable pprof endpoints on the debug listener")

	// WWW Config
	flags.StringVar(&wwwListen, "www", wwwListen, "The address to listen on for incoming WWW requests, overrides $SHOPDB_WWW_LISTEN")
	flags.StringVar(&wwwKey, "www_key", wwwKey, "The path to a PEM encoded private key, overrides $SHOPDB_WWW_KEY")
	flags.StringVar(&wwwCrt, "www_crt", wwwCrt, "The path to a PEM encoded certificate, overrides $SHOPDB_WWW_CRT")

	return &cli.Command{
		Usage:       "serve",
		Description: "Serves web debug information.",
		Flags:       flags,
		Run: func(c *cli.Command, args ...string) (err error) {
			ctx, cancel := context.WithCancel(context.Background())
			go func() {
				select {
				case <-shutdown:
					cancel()
				case <-ctx.Done():
				}
			}()

			err = flags.Parse(args)
			if err != nil {
				return fmt.Errorf("error parsing flags: %q", err)
			}

			// Setup the database
			cfg, err := pgx.ParseConfig(pgDSN)
			if err != nil {
				return fmt.Errorf("error parsing postgres DSN or URI: %v", err)
			}
			cfg.Tracer = &tracelog.TraceLog{Logger: tracelog.LoggerFunc(func(_ context.Context, level tracelog.LogLevel, msg string, data map[string]interface{}) {
				debug.Printf("%s: %s %+v", level, msg, data)
			}), LogLevel: tracelog.LogLevelDebug}
			connStr := stdlib.RegisterConnConfig(cfg)
			db, err := sql.Open("pgx", connStr)
			if err != nil {
				return err
			}
			defer func() {
				if err := db.Close(); err != nil {
					logger.Printf("error closing database connection: %v", err)
				}
			}()

			storedb, err := store.New(ctx, db, sessionKey)
			if err != nil {
				return err
			}
			tmpls, assetsFS, err := getTmpls(message.DefaultCatalog)
			if err != nil {
				return fmt.Errorf("error creating templates: %v", err)
			}
			wwwHandler, wwwMetrics, err := www.New(ctx, www.Config{
				Store:         storedb,
				DisableSignup: true,
				Version:       version,
				Namespace:     Namespace,
				Domain:        domain,
				XSRFKey:       xsrfKey,
				SessionKey:    sessionKey,
				Logger:        logger,
				Debug:         debug,
				C:             message.DefaultCatalog,
				Tmpls:         tmpls,
				Assets:        assetsFS,
			})
			if err != nil {
				return fmt.Errorf("error creating website: %q", err)
			}
			// Setup the debug server
			_, debugServer := newMetrics(usepprof, debugListen, logger, wwwMetrics)

			go func() {
				debug.Printf("listening for metrics and debug on %q", debugListen)
				err := debugServer.ListenAndServe()
				if err != http.ErrServerClosed {
					logger.Printf("debug http server unexpectedly terminated: %q", err)
				}
			}()

			// Listen for incoming WWW connections:
			wwwListener, err := net.Listen("tcp", wwwListen)
			if err != nil {
				return fmt.Errorf("failed to listen for www requests: %q", err)
			}
			wwwListener = netutil.LimitListener(wwwListener, maxWWWAccept)
			debug.Printf("listening for www requests on %q", wwwListen)

			wwwServer := &http.Server{
				Addr:              wwwListen,
				Handler:           wwwHandler,
				ReadHeaderTimeout: readTimeout,
				WriteTimeout:      writeTimeout,
				ErrorLog:          logger,
				TLSConfig: &tls.Config{
					PreferServerCipherSuites: true,
					MinVersion:               tls.VersionTLS12,
				},
			}
			wwwShutdown := make(chan struct{})
			go func() {
				if wwwKey != "" && wwwCrt != "" {
					if err := wwwServer.ServeTLS(wwwListener, wwwCrt, wwwKey); err != http.ErrServerClosed {
						logger.Printf("www server unexpectedly terminated: %q\n", err)
					}
				} else {
					if err := wwwServer.Serve(wwwListener); err != http.ErrServerClosed {
						logger.Printf("www server unexpectedly terminated: %q\n", err)
					}
				}
				close(wwwShutdown)
			}()

			// Listen for incoming GraphQL connections:
			graphListener, err := net.Listen("tcp", graphListen)
			if err != nil {
				return fmt.Errorf("failed to listen for GraphQL requests: %q", err)
			}
			graphListener = netutil.LimitListener(graphListener, maxGraphAccept)

			graphMux := http.NewServeMux()
			srv := handler.NewDefaultServer(graph.NewExecutableSchema(graph.Config{Resolvers: &graph.Resolver{
				Store:  storedb,
				Logger: logger,
				Debug:  debug,
			}}))
			graphMux.Handle("/", playground.Handler("GraphQL playground", "/query"))
			graphMux.Handle("/query", srv)

			graphServer := &http.Server{
				Addr:              graphListen,
				Handler:           graphMux,
				ReadHeaderTimeout: readTimeout,
				WriteTimeout:      writeTimeout,
				ErrorLog:          logger,
			}
			graphShutdown := make(chan struct{})
			go func() {
				debug.Printf("listening for GraphQL requests on %q", graphListen)
				if err := graphServer.Serve(graphListener); err != http.ErrServerClosed {
					logger.Printf("GraphQL server unexpectedly terminated: %q\n", err)
				}
				close(graphShutdown)
			}()

			select {
			case <-shutdown:
				debug.Println("server received shutdown signal, shutting down…")
				break
			case <-graphShutdown:
				debug.Println("GraphQL server terminated, shutting down…")
				break
			case <-wwwShutdown:
				debug.Println("www server terminated, shutting down…")
				break
			}
			defer func() {
				if e := debugServer.Shutdown(context.TODO()); e != nil && err == nil {
					err = fmt.Errorf("error shutting down debug HTTP server: %q", e)
				}
			}()
			defer func() {
				if e := graphServer.Shutdown(context.TODO()); e != nil && err == nil {
					err = fmt.Errorf("error shutting down GraphQL server: %q", e)
				}
			}()
			defer func() {
				if e := wwwServer.Shutdown(context.TODO()); e != nil && err == nil {
					err = fmt.Errorf("error shutting down www HTTP server: %q", e)
				}
			}()
			return err
		},
	}
}
