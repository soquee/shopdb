//go:build dev
// +build dev

package main

import (
	"io/fs"
	"os"

	"golang.org/x/text/message/catalog"

	"code.soquee.net/tmpl"
)

var assets = os.DirFS("assets")

func getTmpls(c catalog.Catalog) (tmpl.Template, fs.FS, error) {
	publicFS, err := fs.Sub(assets, "public")
	if err != nil {
		return tmpl.Template{}, nil, err
	}
	tmplFS, err := fs.Sub(assets, "pages")
	if err != nil {
		return tmpl.Template{}, nil, err
	}
	baseTmplFS, err := fs.Sub(assets, "layouts")
	if err != nil {
		return tmpl.Template{}, nil, err
	}
	tmpls, err := tmpl.New(
		tmpl.FS(tmplFS),
		tmpl.BaseFS(baseTmplFS),
		tmpl.Dev(true),
		tmpl.Catalog(c),
	)
	return tmpls, publicFS, err
}
