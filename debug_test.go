package main

import (
	"net/http"
	"testing"

	"code.soquee.net/testlog"
	"github.com/prometheus/client_golang/prometheus/collectors"
)

func TestNewMetricsRegistry(t *testing.T) {
	metrics, _ := newMetrics(false, "", testlog.New(t))
	func() {
		defer func() {
			if r := recover(); r == nil {
				t.Errorf("expected NewProcessCollector to be registered by default.")
			}
		}()
		metrics.MustRegister(
			collectors.NewProcessCollector(collectors.ProcessCollectorOpts{
				Namespace: Namespace,
			}),
		)
	}()
	func() {
		defer func() {
			if r := recover(); r == nil {
				t.Errorf("expected NewGoCollector to be registered by default.")
			}
		}()
		metrics.MustRegister(
			collectors.NewGoCollector(),
		)
	}()
}

func TestNewMetricsServer(t *testing.T) {
	const (
		debugAddr       = "debug:123"
		metricsEndpoint = "/debug/metrics"
	)

	doTest := func(usepprof bool) {
		_, server := newMetrics(usepprof, debugAddr, testlog.New(t))
		if server.Addr != debugAddr {
			t.Errorf("Got unexpected value for server.Addr: want=%q, got=%q", debugAddr, server.Addr)
		}

		mux := server.Handler.(*http.ServeMux)

		// Check that the metrics handler was registered.
		// This does not check that the correct handler is registered, just that
		// *some* handler is registered.
		func() {
			defer func() {
				if r := recover(); r == nil {
					t.Errorf("Expected handler to be registered at %q when pprof is %t", metricsEndpoint, usepprof)
				}
			}()
			mux.HandleFunc(metricsEndpoint, func(w http.ResponseWriter, req *http.Request) {
				panic("should not be reached")
			})
		}()

		// Check that each endpoint was registered if usepprof is true (or not
		// registered otherwise).
		// This does not check that the correct handler is registered, just that
		// *some* handler is registered.
		for _, v := range []string{
			"/debug/pprof/",
			"/debug/pprof/cmdline",
			"/debug/pprof/profile",
			"/debug/pprof/symbol",
			"/debug/pprof/trace",
		} {
			if usepprof {
				func() {
					defer func() {
						if r := recover(); r == nil {
							t.Errorf("Expected handler to be registered at %q", v)
						}
					}()
					mux.HandleFunc(v, func(w http.ResponseWriter, req *http.Request) {
						panic("should not be reached")
					})
				}()
			} else {
				func() {
					defer func() {
						if r := recover(); r != nil {
							t.Errorf("Did not expect handler to be registered at %q", v)
						}
					}()
					mux.HandleFunc(v, func(w http.ResponseWriter, req *http.Request) {
						panic("should not be reached")
					})
				}()
			}
		}
	}

	t.Run("pprof", func(t *testing.T) {
		doTest(true)
	})
	t.Run("nopprof", func(t *testing.T) {
		doTest(false)
	})
}
