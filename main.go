// The shopdb command runs migrations.
//
// For more information try:
//
//	./shopdb help
package main // import "code.soquee.net/shopdb"

import (
	"flag"
	"io"
	"log"
	"os"
	"os/signal"
	"syscall"

	"code.soquee.net/env"
	"code.soquee.net/shopdb/internal/migrate"
	"mellium.im/cli"
)

// build time configuration
var (
	Version = "devel"
)

func main() {
	// Setup Logging
	logger := log.New(os.Stderr, "", log.LstdFlags)

	fd, err := os.Open(".env")
	if err != nil {
		logger.Printf("Error opening .env file: %q", err)
	} else {
		err = env.Read(fd)
		if err != nil {
			logger.Fatal(err)
		}
		err = fd.Close()
		if err != nil {
			logger.Fatal(err)
		}
	}

	// Setup command line flags
	var (
		verbose = false
		help    = false
		h       = false
	)
	flags := flag.NewFlagSet(os.Args[0], flag.ContinueOnError)
	flags.BoolVar(&verbose, "v", verbose, "Turns on verbose logging")
	flags.BoolVar(&help, "help", help, "Shows this help message")
	flags.BoolVar(&h, "h", h, "Shows this help message")
	err = flags.Parse(os.Args[1:])
	if err != nil {
		logger.Fatal(err)
	}

	// Setup verbose logging
	debug := log.New(io.Discard, "", 0)
	if verbose {
		debug = log.New(os.Stderr, "DEBUG ", 0)
	}

	// Signal handling
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, syscall.SIGQUIT, syscall.SIGTERM)

	shutdown := make(chan struct{})
	go func() {
		select {
		case sig := <-c:
			debug.Printf("received signal %v, shutting down…\n", sig)
			close(shutdown)
		case <-shutdown:
		}
	}()

	// Commands
	cmds := &cli.Command{
		Usage: os.Args[0],
		Flags: flags,
	}
	cmds.Commands = []*cli.Command{
		aboutCmd(os.Stdout, Version, verbose),
		cli.Help(cmds),
		migrate.Cmd(getMigrationsFS(), logger, debug, shutdown),
		serveCmd(Version, logger, debug, shutdown),
		validateCmd(logger),
	}

	if help || h {
		helpCmd := cli.Help(cmds)
		if err := helpCmd.Run(helpCmd); err != nil {
			debug.Printf("error running help command: %v", err)
		}
		return
	}

	err = cmds.Exec(flags.Args()...)
	switch err {
	case flag.ErrHelp:
		// Ignore errors from using an implicitly defined -h or -help flag.
	case cli.ErrInvalidCmd:
		helpCmd := cli.Help(cmds)
		if err := helpCmd.Exec(); err != nil {
			debug.Printf("error running help command: %v", err)
		}
	case cli.ErrNoRun:
		helpCmd := cli.Help(cmds)
		if err := helpCmd.Exec(); err != nil {
			debug.Printf("error running help command: %v", err)
		}
	case nil:
	default:
		logger.Println(err)
	}
}
