package main

import (
	"bytes"
	"os"
	"strings"
	"testing"
)

func TestAboutName(t *testing.T) {
	defer func() {
		if r := recover(); r == nil {
			t.Errorf("expected aboutCmd with nil writer to panic")
		}
	}()

	cmd := aboutCmd(nil, "version", false)
	if name := cmd.Name(); name != "about" {
		t.Errorf("wrong name: got=%s, want=about", name)
	}
}

func TestAboutOutput(t *testing.T) {
	buf := new(bytes.Buffer)
	cmd := aboutCmd(buf, "versionabcd", false)
	if err := cmd.Run(cmd); err != nil {
		t.Errorf("unexpected error: `%v'", err)
	}

	bufStr := buf.String()
	if !strings.Contains(bufStr, os.Args[0]) {
		t.Errorf("expected output to contain `%s', got:\n`%s'\n", os.Args[0], bufStr)
	}
	if !strings.Contains(bufStr, "versionabcd") {
		t.Errorf("expected output to contain version, got:\n`%s'\n", bufStr)
	}
}
