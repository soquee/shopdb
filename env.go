package main

import (
	"os"
	"strconv"
)

func getEnvOrDef(key, def string) string {
	val := os.Getenv(key)
	if val == "" {
		val = def
	}
	return val
}

func getEnvBool(key string) bool {
	e := os.Getenv(key)
	if e == "" {
		return false
	}
	b, err := strconv.ParseBool(e)
	if err != nil {
		return false
	}
	return b
}
